library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SoundDetector_tb is
end SoundDetector_tb;

architecture Behavioral of SoundDetector_tb is

component SoundDetector is
  Port (
        clk : in std_logic := '0';
        en : in std_logic := '1';
        error : out std_logic := '0';
        sound : in std_logic_vector(15 downto 0)
        );
end component SoundDetector;

    constant clkPeriod : time := 10 ns;
    signal clk : std_logic := '0';
    signal en : std_logic := '1';
    signal error : std_logic := '0';
    signal sound : std_logic_vector(15 downto 0);

begin
    
    clk <= not clk after clkPeriod / 2;
    soundDetectorIns0 : SoundDetector port map (clk, en, error, sound);

process
begin
sound <= "0000000001000111";
wait for 1 ps;
wait for clkPeriod;
sound <= "0000000001001000";
wait for clkPeriod;
sound <= "0000000001001000";
wait for clkPeriod;
sound <= "0000000001001001";
wait for clkPeriod;
sound <= "0000000001001010";
wait for clkPeriod;
sound <= "0000000001001010";
wait for clkPeriod;
sound <= "0000000001001011";
wait for clkPeriod;
sound <= "0000000001001011";
wait for clkPeriod;

sound <= "0000000001000111";
wait for clkPeriod;
sound <= "0000000001001000";
wait for clkPeriod;
sound <= "0000000001001000";
wait for clkPeriod;
sound <= "0000000001001001";
wait for clkPeriod;
sound <= "0000000001001010";
wait for clkPeriod;
sound <= "0000000001001010";
wait for clkPeriod;
sound <= "0000000001001011";
wait for clkPeriod;
sound <= "0000000001001011";
wait for clkPeriod;

sound <= "0000010000000000";
wait for clkPeriod;
sound <= "0000000001100101";
wait for clkPeriod;
sound <= "0000010000000000";
wait for clkPeriod;
sound <= "0000001110011011";
wait for clkPeriod;
sound <= "0000010000000000";
wait for clkPeriod;
sound <= "0000000001100101";
wait for clkPeriod;
sound <= "0000010000000000";
wait for clkPeriod;
sound <= "0000001110011011";

end process;

end Behavioral;
