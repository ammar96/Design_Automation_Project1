library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
entity SoundDetector is
  Port (
        clk : in std_logic := '0';
        en : in std_logic := '1';
        error : out std_logic := '0';
        sound : in std_logic_vector(15 downto 0)
        );
end SoundDetector;
architecture Behavioral of SoundDetector is
COMPONENT xfft_0
  PORT (
    aclk : IN STD_LOGIC;
    aclken : IN STD_LOGIC;
    aresetn : IN STD_LOGIC;
    s_axis_config_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_config_tvalid : IN STD_LOGIC;
    s_axis_config_tready : OUT STD_LOGIC;
    s_axis_data_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axis_data_tvalid : IN STD_LOGIC;
    s_axis_data_tready : OUT STD_LOGIC;
    s_axis_data_tlast : IN STD_LOGIC;
    m_axis_data_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axis_data_tvalid : OUT STD_LOGIC;
    m_axis_data_tready : IN STD_LOGIC;
    m_axis_data_tlast : OUT STD_LOGIC;
    event_frame_started : OUT STD_LOGIC;
    event_tlast_unexpected : OUT STD_LOGIC;
    event_tlast_missing : OUT STD_LOGIC;
    event_status_channel_halt : OUT STD_LOGIC;
    event_data_in_channel_halt : OUT STD_LOGIC;
    event_data_out_channel_halt : OUT STD_LOGIC
  );
END COMPONENT;
  function getIntValue (a : std_logic_vector(15 downto 0)) return integer is
	variable retVal: integer;
  begin
	if (a(15) = '0') then
		retVal := to_integer(unsigned(a));
	else
		retVal := to_integer(unsigned(a)) - 2 ** 16;
	end if;
	return retVal;
  end getIntValue;
  
  function getMagnitude (a : std_logic_vector(31 downto 0)) return integer is
      variable retVal: integer;
    begin
      retVal := getIntValue(a(31 downto 16)) ** 2 + getIntValue(a(15 downto 0)) ** 2;
      return retVal;
    end getMagnitude;
    
    function checkError (prev: integer; cur: integer) return std_logic is
        variable retVal: std_logic;
      begin
        if (abs(cur - prev) > 50000) then
            retVal := '1';
        else
            retVal := '0';
        end if;
        return retVal;
      end checkError;
    
  signal aresetn                     : std_logic := '1';  -- synchronous active low reset
  signal aclken                      : std_logic := '1';  -- clock enable
  -- Config slave channel signals
  signal s_axis_config_tvalid        : std_logic := '1';  -- payload is valid
  signal s_axis_config_tready        : std_logic := '1';  -- slave is ready
  signal s_axis_config_tdata         : std_logic_vector(7 downto 0) := (others => '0');  -- data payload
  -- Data slave channel signals
  signal s_axis_data_tvalid          : std_logic := '0';  -- payload is valid
  signal s_axis_data_tready          : std_logic := '0';  -- slave is ready
  signal s_axis_data_tdata           : std_logic_vector(31 downto 0) := (others => '0');  -- data payload
  signal s_axis_data_tlast           : std_logic := '0';  -- indicates end of packet
  -- Data master channel signals
  signal m_axis_data_tvalid          : std_logic := '0';  -- payload is valid
  signal m_axis_data_tready          : std_logic := '1';  -- slave is ready
  signal m_axis_data_tdata           : std_logic_vector(31 downto 0) := (others => '0');  -- data payload
  signal m_axis_data_tlast           : std_logic := '0';  -- indicates end of packet
  -- Event signals
  signal event_frame_started         : std_logic := '0';
  signal event_tlast_unexpected      : std_logic := '0';
  signal event_tlast_missing         : std_logic := '0';
  signal event_status_channel_halt   : std_logic := '0';
  signal event_data_in_channel_halt  : std_logic := '0';
  signal event_data_out_channel_halt : std_logic := '0';
  
  subtype sound_t is std_logic_vector(15 downto 0);
  type allSounds_t is array(7 downto 0) of sound_t;
begin
xfft_0_ins0 : xfft_0
  PORT MAP (
    aclk => clk,
    aclken => aclken,
    aresetn => aresetn,
    s_axis_config_tdata => s_axis_config_tdata,
    s_axis_config_tvalid => s_axis_config_tvalid,
    s_axis_config_tready => s_axis_config_tready,
    s_axis_data_tdata => s_axis_data_tdata,
    s_axis_data_tvalid => s_axis_data_tvalid,
    s_axis_data_tready => s_axis_data_tready,
    s_axis_data_tlast => s_axis_data_tlast,
    m_axis_data_tdata => m_axis_data_tdata,
    m_axis_data_tvalid => m_axis_data_tvalid,
    m_axis_data_tready => m_axis_data_tready,
    m_axis_data_tlast => m_axis_data_tlast,
    event_frame_started => event_frame_started,
    event_tlast_unexpected => event_tlast_unexpected,
    event_tlast_missing => event_tlast_missing,
    event_status_channel_halt => event_status_channel_halt,
    event_data_in_channel_halt => event_data_in_channel_halt,
    event_data_out_channel_halt => event_data_out_channel_halt
  );
    s_axis_data_tdata(31 downto 16) <= (others => '0');
process (en, clk)

    variable allSounds : allSounds_t;
    variable inSoundCounter : integer range 0 to 10 := 0;
    variable outSoundCounter : integer range 0 to 10 := 0;
    variable prevSum : integer := 0;
    variable sum : integer := 0;
    variable firstSample : boolean := true;
    
begin
    if (en = '1') then
        aresetn <= '1';
        m_axis_data_tready <= '1';
    else
        aresetn <= '0';
        s_axis_data_tvalid <= '0';
        m_axis_data_tready <= '0';
    end if;
    if (en = '1' and rising_edge(clk)) then
        if (inSoundCounter <= 7) then --save input sounds in 2D matrix.
            allSounds(inSoundCounter) := sound;
            inSoundCounter := (inSoundCounter + 1) mod 8;
        else
            null;
        end if;
        if (s_axis_data_tready = '1') then--wait for FFT module to get ready.
            if (outSoundCounter < 7) then --pass saved sounds to FFT module.
                if (outSoundCounter = 0) then
                    if (not firstSample) then
                        error <= checkError(prevSum, sum);
                    end if;
                    firstSample := false;    
                    prevSum := sum;
                    sum := 0;
                end if;
                s_axis_data_tvalid <= '1';
                s_axis_data_tdata(15 downto 0) <= allSounds(outSoundCounter);
                outSoundCounter := outSoundCounter + 1;
            elsif (outSoundCounter = 7) then
                s_axis_data_tlast <= '1'; --this is the last sound of the frame.
                outSoundCounter := outSoundCounter + 1;
            else
                s_axis_data_tlast <= '0';
                s_axis_data_tvalid <= '0';
                outSoundCounter := 0;
            end if;
	    end if;
	    if (m_axis_data_tvalid = '1') then--wait for FFT module to ready the results;
            sum := sum + getMagnitude(m_axis_data_tdata);
        end if;
    end if;
end process;
end Behavioral;